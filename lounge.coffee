if Meteor.isClient
  Meteor.subscribe 'rooms'
  Meteor.subscribe 'messages', room: Session.get 'currentRoomId'
  
  #Views
  Template.contacts.user = ->
    Meteor.users.find {}

  Template.conversation.message = ->
    Messages.find {room: Session.get 'currentRoomId'}
    
  Template.inbox.room = ->
    Rooms.find {}
    
  Template.inbox.events(
    'click .room-link': ->
      Session.set 'currentRoomId', this._id
      console.log 'Switched room'
  )
    
  Template.message.events(
    'click .remove-message': ->
      Messages.remove this._id
      console.log 'Message removed'
  )
  
  Template.messageForm.events(
    'click #new-message-button': ->
      addMessage()
    'keydown': (e) ->
      if (e.keyCode or e.which) is 13
        if $('#post-on-enter').is(':checked')
          addMessage()
  )
 
  addMessage = -> 
    Messages.insert(
      author: Meteor.user()
      room: Session.get 'currentRoomId'
      text: $('#new-message-text').val()
      createdAt = new Date()
      tags: []
    )
    $("html, body").animate scrollTop: $(document).height()
    console.log 'Message added'
    $('#new-message-text').val('').focus()
  
  Template.newRoomDialog.events(
    'click #new-room-create': ->
      Rooms.insert(
        owner: Meteor.user()
        title: $('#new-room-title').val()
        public: $('#new-room-public').is(':checked')
        open: true
        createdAt = new Date()
      )
      console.log 'Room created'
      $('#new-message-text').val('').focus()
  )
  
  Template.newRoomDialog.helpers(
    userSource: ->
      users = []
      all = Meteor.users.find {}
      all.forEach (u) ->
        users.push('"' + u.username + '"')
      "[" + users + "]"
    userUpdater: (item) ->
        console.log(item)
        item
  )
  
  Template.inbox.events(
    'click .remove-room': ->
      Messages.remove(
        room: this._id
      )
      Rooms.remove this._id
      console.log 'Room removed'
  )
  
  Template.inbox.helpers(
    isCurrentRoom: ->
      this._id == Session.get 'currentRoomId'
  )
  
  Template.conversation.helpers(
    currentRoomId: ->
      Session.get 'currentRoomId'
  )
  ###
  $(document).ready( ->
    $('#toggle-inbox').sidr(
      name: 'test'
      side: 'left'
    )
    $('#toggle-contacts').sidr(
      name: 'contacts'
      side: 'right'
    )
  )
  ###

  Template.newRoomDialog.rendered = ->
    $('#new-room-participants').typeahead(
      #minLength: 1
      #property : 'username'
      source: ->
        users = []
        all = Meteor.users.find {}
        all.forEach (u) ->
          users.push(u.username)
        users
        
      updater: (item) ->
        $('#invited').append(item, ', ')
        $('#new-room-participants').val('')
        console.log(item + ' added')
        item
      ###        
      matcher: (item) ->
        item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1
          
      sorter: (items) ->
        items.sort()
          
      highlighter: (item) ->
        regex = new RegExp( '(' + this.query + ')', 'gi' )
        item.replace( regex, "<strong>$1</strong>" )
      ###
    )


if Meteor.isServer
  Meteor.startup ->
    #Rooms.remove({})
