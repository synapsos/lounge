# All Messages -- data model
# Loaded on both the client and the server

###
// Messages

  Each message is represented by a document in the Messages collection:
    author: user id
    room: room id
    text: String
    tags: Array of tags
  Methods:
    delete
###

Messages = new Meteor.Collection 'messages'

