# All Rooms -- data model
# Loaded on both the client and the server

###
// Rooms

  Each room is represented by a document in the Rooms collection:
    owner: user id
    title, description: String
    public: Boolean - Everyone can join / Has to be invited
    open: Boolean - Users can post / Room closed
    users: Array of user id's that are participating
    
  Methods:
    create
    close
    delete
    invite
    subscribe
    unsubscribe
###

Rooms = new Meteor.Collection 'rooms'

###
Rooms.allow(
  insert: (userId, doc) ->
    # the user must be logged in, and the document must be owned by the user
    (userId && doc.owner == userId)
    
  update: (userId, doc, fields, modifier) ->
    # can only change your own documents
    doc.owner == userId
    
  remove: (userId, doc) ->
    # can only remove your own documents
    doc.owner == userId;
    
  fetch: ['owner']
)

Rooms.deny(
  update: (userId, docs, fields, modifier) ->
    # can't change owners
    _.contains(fields, 'owner')
    
  remove: (userId, doc) ->
    # can't remove locked documents
    doc.locked
  fetch: ['locked'] # no need to fetch 'owner'
)
###
